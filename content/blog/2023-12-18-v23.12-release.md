title: "v23.12: The One We Asked The Community To Name"
title-short: "v23.12"
date: 2023-12-18
preview: "2023-12/v23.12.webp"
---

[![A big variation of the postmarketOS logo, around it are phones with Sxmo, Plasma Mobile, Phosh and GNOME Shell on Mobile; the phones are lying on top of lots of postmarketOS stickers on a piece of cardboard.](/static/img/2023-12/v23.12.webp){: class="wfull border" }](/static/img/2023-12/v23.12.webp)

The year is coming to a close. As always, it is the time to reflect and to get
the second stable postmarketOS release of the year done. postmarketOS v23.12 is
based on the excellent
[Alpine Linux 3.19](https://alpinelinux.org/posts/Alpine-3.19.0-released.html).
Besides upgrading everything to new versions, for the first time we have
support for a lot of Chromebooks and
[MSM8953](https://wiki.postmarketos.org/wiki/Qualcomm_Snapdragon_450/625/626/632_(MSM8953))-based
Xiaomi devices!

v23.12, like previous releases, is geared mainly towards Linux
enthusiasts as explained in our article [state of postmarketOS](/state).

## Supported Devices

The amount of supported devices has been increased to 45 (from 31 since
[v23.06](/blog/2023/06/07/v23.06-release/)).

[#grid side#]

[![Photo of the google-peach-pit chromebook](/static/img/2023-12/Google-peach-pit-mate.jpg){: class="w300 border" }](https://wiki.postmarketos.org/wiki/File:Google-peach-pit-mate.jpg)
<br><span class="w300">
postmarketOS v23.12 can be used to breathe new life into the Samsung
Chromebook 2 11.6" and many other Chromebooks for the first time!
</span>

[![Photo of a bunch of MSM8953 devices](/static/img/2023-12/msm8953.webp){: class="w300 border" }](/static/img/2023-12/msm8953.webp)
<br><span class="w300">
A typical gathering of MSM8953 devices running postmarketOS v23.12.
</span>

[#grid text#]

* ASUS MeMO Pad 7
* Arrow DragonBoard 410c
* BQ Aquaris X5
* Fairphone 4
* Google Chromebooks with x64 CPU {{new}}
* Google Gru Chromebooks {{new}}
* Google Kukui Chromebooks {{new}}
* Google Oak Chromebooks {{new}}
* Google Trogdor Chromebooks {{new}}
* Google Veyron Chromebooks {{new}}
* Lenovo A6000
* Lenovo A6010
* Motorola Moto G4 Play
* Nokia N900
* ODROID XU4 {{new}}
* OnePlus 6
* OnePlus 6T
* PINE64 PinePhone
* PINE64 PinePhone Pro
* PINE64 Pinebook Pro
* PINE64 RockPro64
* Purism Librem 5
* SHIFT6mq
* Samsung Chromebook {{new}}
* Samsung Chromebook 2 11.6" {{new}}
* Samsung Galaxy A3 (2015)
* Samsung Galaxy A5 (2015)
* Samsung Galaxy E7
* Samsung Galaxy Grand Max
* Samsung Galaxy S III (GT-I9300 and SHW-M440S)
* Samsung Galaxy S4 Mini Value Edition
* Samsung Galaxy Tab 2 7.0"
* Samsung Galaxy Tab 2 10.1"
* Samsung Galaxy Tab A 8.0
* Samsung Galaxy Tab A 9.7
* Wileyfox Swift
* Xiaomi Mi A1 {{new}}
* Xiaomi Mi A2 Lite {{new}}
* Xiaomi Mi Note 2
* Xiaomi Pocophone F1
* Xiaomi Redmi 2
* Xiaomi Redmi 4 Prime {{new}}
* Xiaomi Redmi 5 Plus {{new}}
* Xiaomi Redmi Note 4 {{new}}
* Xiaomi Redmi S2/Y2 {{new}}

[#grid end#]

The ODROID HC2 is not available in v23.12 anymore. It can still be used with
postmarketOS edge. If you are interested in getting it back into the stable
release, the first step would be you stepping up to maintain the port - let us
know in the issues or chat.

### User Interfaces

<!-- ordered alphabetically -->

* [GNOME Shell on Mobile 45_git20230908](https://release.gnome.org/45/)
  replaces 44_git20230405 from v23.06

* [Phosh 0.33](https://phosh.mobi/releases/rel-0.33.0/)
  replaces version 0.30 from v23.06 SP1.

* [Plasma 5.27.10](https://kde.org/announcements/plasma/5/5.27.10/)
  replaces version 5.27.8 from v23.06.

* [Sxmo 1.15.1](https://lists.sr.ht/~mil/sxmo-announce/%3CCXIQYCSF6FI9.28OPMYXGT91GL%40willowbarraco.fr%3E)
  replaces version 1.14.0 from v23.06.

## Notable Changes

* Unlocking of encrypted installations is now done with
  [unl0kr](https://gitlab.com/cherrypicker/unl0kr) by default, the
  successor to osk-sdl ([more](/edge/2023/10/04/osk-sdl-deprecated/)).

* The default USB networking gadget was changed from RNDIS to NCM
  ([more](/edge/2023/10/29/rndis-ncm/)).

* The default image viewer in Phosh and GNOME is now Loupe, instead of
  previously Eye of GNOME (EoG) ([more](/edge/2023/08/24/eog-replaced-with-loupe/))

* The [release upgrade process](https://wiki.postmarketos.org/wiki/Upgrade_to_a_newer_postmarketOS_release)
  has been made more robust. There are additional safety checks for a too small
  boot partition and for having hardcoded versions or packages installed via
  [mrtest](https://wiki.postmarketos.org/wiki/Mrtest). Additionally a bug was
  fixed that caused Alpine mirrors without `/alpine/` in the URL to not get
  properly replaced during the upgrade.

* Installation images have been removed. We used to provide these for the
  PinePhone (Pro), Librem 5 and Pinebook Pro as a convenient method to install
  postmarketOS encrypted without building your own image with pmbootstrap
  first, and to install from micro SD cards to the internal storage. A rewrite
  of the installer code has been in progress for quite some time, with the goal
  to support more devices and to have a more automated testing. The new code
  base is not ready yet however, and due to regressions with the old code base
  it was not feasible to keep the installer images for v23.12. So it was
  decided to adjust the installation instructions in the wiki for the affected
  devices to explain how the same types of installations can be accomplished
  without the installer images for v23.12.

* ZRAM can be enabled via the following command. We plan to enable it
  automatically once it is more tested in edge ({{MR|4598|pmaports}}).
```
# rc-update add zram-init default
```

## Testing And Known Issues

A *huge thank you* to device maintainers and the testing team, and people who
spontaneously decided to take part in testing this new release
({{issue|2409|pmaports}}) and fixing bugs right before finalizing it! If you
would like to join the fun next time, add yourself to the
[Testing Team](https://wiki.postmarketos.org/wiki/Testing_Team).

Notable regressions:

* {{issue|15|postmarketos-release-upgrade}}: we got one report of getting stuck
  at boot with `unsupported 'crypto_LUKS' filesystem`. This was after upgrading
  a PinePhone install with FDE. If you are also affected, please report back
  and see the issue for workarounds.
* {{issue|2434|pmaports}}: shift-axolotl: boot fails with FDE
* {{issue|2407|pmaports}}: shift-axolotl: audio profiles broken

Notable issues with new device ports:

* {{issue|2435|pmaports}}: google-oak: audio is broken
* {{issue|2375|pmaports}}: google-veyron: USB port dead after resume/wakeup

## How To Get It

### New Installation

For new installs, see [download](/download) and make sure to read the wiki page
for [your device](https://wiki.postmarketos.org/wiki/Devices).

### Upgrade

For existing installations, see the
[upgrade to a newer postmarketOS release](https://wiki.postmarketos.org/wiki/Upgrade_to_a_newer_postmarketOS_release)
wiki article.

Recommended manual steps after upgrading:

* [Replace osk-sdl with unl0kr](/edge/2023/10/04/osk-sdl-deprecated/)
* [Replace eog with loupe](/edge/2023/08/24/eog-replaced-with-loupe/)

## The Road Ahead
Members of the postmarketOS community and wider Linux Mobile scene can be found
at [37C3](https://linmob.net/37c3-assembly/) and
[FOSDEM 2024](https://fosdem.org/2024/) (see the related stand and devroom). We
have a lot of plans for postmarketOS for 2024 and beyond, towards making it
more reliable and more! To accelerate that development, consider
[contributing](https://wiki.postmarketos.org/wiki/Contributing) or
[making a donation](/blog/2023/12/14/pmos-on-oc/).

## A Community Effort

A big thanks to everybody who contributed to postmarketOS, to Alpine or to any
of the numerous upstream components we use — without you this would not be
possible!
