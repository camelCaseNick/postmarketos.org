title: "postmarketOS needs your help!"
title-short: "postmarketOS needs your help!"
date: 2023-12-14
---

We'd like to announce that [postmarketOS is now accepting donations on Open
Collective](https://opencollective.com/postmarketos)!

This is a big change for us; until now we've accepted donations but have
largely discouraged them because frankly we've had little use for them. We
thought it was somewhat pointless to ask folks for donations and have the money
sitting around waiting for something to happen with it...

But times are changing! And we now have some pretty good reasons for accepting
donations!

### What has changed?

postmarketOS now has one developer working full-time (our very own
[craftyguy](https://freeradical.zone/@craftyguy/111506290148662542)),
with the likelihood that more will join in the future. Donations will be used
to pay developers for their work, and it will have a direct impact on how fast
postmarketOS development can go.

[Our testing team](https://postmarketos.org/blog/2023/05/21/call-for-testers/)
is excellent, and we would like to support them further by designing new
infastructure for automated testing/CI on real devices that folks want to use
with postmarketOS. Donations will be used to pay for this new infrastructure
and its ongoing maintenance.

"Hackathons", where we bring postmarketOS developers from all over to one
physical location to work on postmarketOS together, have been extremely valuable
for accelerating postmarketOS development. Donations will be used to fund these
events and associated travel costs.

### Why Open Collective?

All contributions and expenses are public on Open Collective, so our finances
are completely transparent. Anyone at any time can see how we are funded and
what we are spending money on.

Our fiscal host on the platform is Open Collective Europe. We chose them
because we believe their values/goals are closely aligned with ours and they
have experience hosting other established free software project. Open
Collective Europe allows us to operate as a non-profit organization (NPO) and
frees up some of our time from having to manage finances. 

Going forward, this is the preferred way to contribute financially to
postmarketOS.

We'd also like to extend a **big** thank you to NLnet, our previous fiscal
host, for their many years of service!

As postmarketOS continues to grow, we must adapt to enable ourselves to scale
up both as a stabilising distribution, and as a community of users and
developers who want to actually own the hardware they paid for.
By donating to us, you contribute to our long term sustainability and help us
prove that "free" software doesn't have to come with a catch.

Please consider donating on [our Open Collective
page](https://opencollective.com/postmarketos) if you can, your help is greatly
appreciated!
