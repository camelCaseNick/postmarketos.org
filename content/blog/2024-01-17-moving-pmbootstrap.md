title: "Moving pmbootstrap"
title-short: "Moving pmbootstrap"
date: 2024-01-17
---

For the last ~18 months we've used SourceHut for pmbootstrap development to
evaluate potentially making it the home for all of postmarketOS development.

We originally laid out some important features that we would need to make this a
reality, the biggest one being able to review patches with a web UI where one is
able to post comments, make suggestions, etc, (similar to how it works on other
forges). Neither the SourceHut team nor we had time to work on that.

During this time we have also been trying out the e-mail based workflow. It has
its advantages, and obviously it works extremely well for some projects. But for
the postmarketOS team it has mostly increased friction.

So for now we decided to move pmbootstrap back over to GitLab.com, where the
rest of our source code repositories are, and we are in the process of finding
an alternative hosting solution that will either be a GitLab instance or a
similar alternative. We are sorry for the back and forth on this issue, and
appreciate everybody that contributed to the discussion.
