title: "Alsa 1.2.10 upgrade will likely cause audio issues"
date: 2023-10-03
---

Alsa 1.2.10 will likely soon be merged in Alpine Edge. Alsa upgrades often break
audio for various devices in postmarketOS, so if you against recommendations
rely on Edge to be reliable it's advised to avoid this upgrade until we've had
time to deal with any potential fallout. On other hand, if you want to help out,
go ahead and do the upgrade and report back if you have issues (make sure no one
else has reported them first, though).

 - [Report and search for issues here](https://gitlab.com/postmarketOS/pmaports/-/issues)
 - [Alsa 1.2.10 merge request in Alpine](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/50906)
