title: "osk-sdl deprecated in favor of unl0kr"
date: 2023-10-04
---

We are happy to announce that [`unl0kr`](https://gitlab.com/cherrypicker/unl0kr),
the successor to `osk-sdl` is now getting installed by default in postmarketOS
edge!

Advantages of `unl0kr`:

* Initramfs is way smaller and much builds faster since we don't need to
  include all of mesa anymore (does not depend on GPU acceleration).
* The unlock screen shows up faster at boot.
* Switching on-screen keyboard layout at runtime (currently supported layouts:
  de, es, fr, us).
* Switching between light and dark theme at runtime.
* Disclosing and hiding entered password at runtime.


### Migrating existing installations

As of writing, postmarketOS still has both osk-sdl and unl0kr in the
repositories but we will remove osk-sdl at some point. If you have an existing
installation, you can run the following commands to switch over to unl0kr.
Consider creating a [backup](https://wiki.postmarketos.org/wiki/Backup_and_restore_your_data)
of your data if you have anything important on your phone, just in case.

```
# apk del osk-sdl
World updated, but the following packages are not removed due to:
  osk-sdl: postmarketos-base postmarketos-ui-console device-qemu-amd64

After this operation, 0 B of additional disk space will be used.
OK: 347 MiB in 244 packages
```

and

```
# apk add unl0kr
The following packages will be REMOVED:
  osk-sdl (possibly more depending on the UI you have installed)
The following NEW packages will be installed:
  inih libevdev libinput-libs libinput-udev libxkbcommon mtdev unl0kr xkeyboard-config
After this operation, XXX MiB of disk space will be freed.
Do you want to continue [Y/n]? Y
(1/41) Purging osk-sdl (0.67.1-r5)
...
21:25:50.230118 == Generating initramfs ==
...
21:25:51.244195 boot-deploy completed in: 0.16s
21:25:51.245401 mkinitfs completed in: 1.02s
OK: 181 MiB in 219 packages
```

### Reporting bugs

unl0kr has been tested on various phones already, see {{issue|1411|pmaports}}.
If it does not work on your phone, and there is no existing issue, then please
create a new one
[here](https://gitlab.com/cherrypicker/unl0kr/-/issues). As always, keep in
mind that this piece of software is developed in spare time and you might need
to help yourself if things don't work (see also
[state of postmarketOS](/state)).

### Removal of osk-sdl from postmarketOS

Currently osk-sdl is still in postmarketOS edge and the stable version. We plan
to remove it from edge after v23.12 was released, so for stable users it would
still be available until v24.06. See {{issue|2319|pmaports}} for details.
