title: "GNOME 42: more upstream, some breakage"
date: 2022-04-19
---

Last week the newest GNOME release, [42](https://release.gnome.org/42),
was [merged](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/32412)
into Alpine Linux. This release comes with lots of improvements! Most notably for
the phone ecosystem, many core apps were upgraded to use GTK4 and libadwaita.
This means that the adaptability is greatly improved, and the number of patches
needed downstream continues shrinking. Unfortunately, it also means that some
improvements which were not pushed upstream are now lost, as we were forced
to [drop them](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3072).
Additionally, those apps upgraded to GTK4 saw a complete UI overhaul and may
have seen very little testing in narrow displays. Therefore, some breakage
is certainly expected. Please report regressions, together with screenshots in
[pma#1479](https://gitlab.com/postmarketOS/pmaports/-/issues/1479)

Also have in mind that to get the whole GNOME 42 experience, an
`# apk upgrade -a` is necessary to replace the dropped forks by the packages
from alpine.
